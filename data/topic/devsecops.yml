title: DevSecOps
seo_title: What is DevSecOps?
description: Effectively turn your DevOps methodology into a DevSecOps methodology
header_body: >-
  Can your existing DevSecOps and application security keep pace with modern
  development methods? Learn how next-generation software requires a new
  approach to app sec.


  [Download the ebook →](https://about.gitlab.com/resources/ebook-ciso-secure-software/)
canonical_path: /topics/devsecops/
file_name: devsecops
twitter_image: /images/opengraph/gitlab-blog-cover.png
related_content:
  - title: What is fuzz testing?
    url: /topics/devsecops/what-is-fuzz-testing/
  - title: What is developer-first security?
    url: /topics/devsecops/what-is-developer-first-security/
  - title: A DevSecOps security checklist
    url: /topics/devsecops/devsecops-security-checklist/
  - title: A beginner's guide to container security
    url: https://about.gitlab.com/topics/devsecops/beginners-guide-to-container-security/
  - url: https://about.gitlab.com/topics/devsecops/three-steps-to-better-devsecops/
    title: Three steps to better DevSecOps
cover_image: /images/topics/application-security.svg
body: >-
  ## What is DevSecOps?


  Security has traditionally come at the end of the development lifecycle, adding cost and time when code is inevitably sent back to the developer for fixes. DevSecOps — a combination of development, security, and operations — is an approach to software development that integrates security throughout the development lifecycle.


  ## DevSecOps vs DevOps


  [DevOps](/topics/devops/) combines development and operations to increase the efficiency, speed, and security of software development and delivery compared to traditional processes. A more nimble software development lifecycle results in a competitive advantage for businesses and their customers. DevOps can be best explained as people working together to conceive, build, and deliver secure software at top speed. DevOps practices enable software developers (devs) and operations (ops) teams to accelerate delivery through automation, collaboration, fast feedback, and iterative improvement.


  Although the term *DevSecOps* looks like *DevOps* with the *Sec* inserted in the middle, it’s more than the sum of its parts. DevSecOps is an evolution of DevOps that weaves application security practices into every stage of software development right through deployment with the use of tools and methods to protect and monitor live applications. New attack surfaces such as containers and orchestrators must be monitored and protected alongside the application itself. DevSecOps tools automate security workflows to create an adaptable process for your development and security teams, improving collaboration and breaking down silos. By embedding security into the software development lifecycle, you can consistently secure fast-moving and iterative processes, improving efficiency without sacrificing quality.


  ![Cost savings of DevSecOps](/images/devsecops/cost-savings-to-shift-left.png)


  ## What is application security?


  Application security is [the use of software, hardware, and procedural methods to protect applications from external threats](https://searchsoftwarequality.techtarget.com/definition/application-security). Modern approaches include *[shifting left](https://about.gitlab.com/blog/2020/06/23/efficient-devsecops-nine-tips-shift-left/)*, or finding and fixing vulnerabilities earlier in the software development process, as well as *shifting right* to secure applications and their infrastructure-as-code in production. Securing the software development lifecycle itself is often a requirement as well.


  This approach of building security processes into your development and operational processes effectively turns your DevOps methodology into a DevSecOps methodology. An end-to-end DevOps platform can best enable this approach.



  ## DevSecOps fundamentals


  If you’ve read the book that was the genesis for the DevOps movement, The Phoenix Project, you understand the importance of automation, consistency, metrics, and collaboration. For DevSecOps, you are essentially applying these techniques to outfit the software factory while embedding security capabilities along the way rather than in a separate, siloed process. Both developers and security teams can find vulnerabilities, but developers are usually required to fix these flaws. It makes sense to empower them to find and fix vulnerabilities while they are still working on the code. Scanning alone isn’t enough. It’s about getting the results to the right people, at the right time, with the right context for quick action.


  Fundamental DevSecOps requirements include automation and collaboration, along with policy guardrails and visibility.



  ## Automation


  GitLab’s [2022 DevSecOps Survey](https://about.gitlab.com/developer-survey/) found that a majority of DevOps teams are running static application security testing (SAST), dynamic application security testing (DAST), or other security scans regularly, but fewer than a third of developers actually get those results in their workflow. A majority of security pros say their DevOps teams are shifting left, and 47% of teams report full test automation.


  ### Collaboration


  A single source of truth that facilitates code review and reports vulnerabilities and remediation provides much-needed transparency to both development and security teams. It can streamline cycles, eliminate friction, and remove unnecessary translation across tools.


  ### Policy guardrails


  Every enterprise has a different appetite for risk. Your security policies will reflect what is right for you while the regulatory requirements to which you must adhere will also influence the policies you must apply. Hand-in-hand with automation, guardrails can ensure consistent application of your security and compliance policies.


  ### Visibility


  An end-to-end DevSecOps platform can give auditors a clear view into who changed what, where, when, and why from beginning to end of the software lifecycle. Leveraging a single source of truth can also ensure earlier visibility into application security risks.


  ## Benefits of DevSecOps


  ### Proactively find and fix vulnerabilities


  Unlike traditional approaches where security is often left to the end, DevSecOps shifts security to earlier in the software development lifecycle. By reviewing, scanning, and testing code for security issues throughout the development process, teams can identify security concerns proactively and address them immediately, before additional dependencies are introduced or code is released to production — and customers.


  ### Release more secure software, faster


  If security vulnerabilities aren’t detected until the end of a project, the result can be major delays as development teams scramble to address the issues at the last minute. But with a DevSecOps approach, developers can remediate vulnerabilities and security flaws while they’re coding, which teaches secure code writing and reduces back and forth during security reviews. Not only does this help organizations release software faster and improve their time to market, it ensures that their software is more secure and cost efficient.


  ### Keep pace with modern development methods


  Customers and business stakeholders demand software that is fast, reliable, and secure. To keep up, development teams need to leverage the latest in collaborative and security technology, including automated security testing, continuous integration and continuous delivery (CI/CD), and vulnerability patching. DevSecOps is all about improving collaboration between development, security, and operations teams to improve organizational efficiency and free up teams to focus on work that drives value for the business.


  ## Is DevSecOps right for your team?


  The benefits of DevSecOps are clear: speed, efficiency, and collaboration. But how do you know if it’s right for your team? If your organization is facing any of the following challenges, a DevSecOps approach might be a good move:


  1. **Development, security, and operations teams are siloed.** If development and operations are isolated from security issues, they can’t build secure software. And if security teams aren’t part of the development process, they can’t identify risks proactively. DevSecOps brings teams together to improve workflows and share ideas. Organizations might even see improved employee morale and retention.

  2. **Long development cycles are making it difficult to meet customer or stakeholder demands.** One reason for the struggle could be security. DevSecOps implements security at every step of the development lifecycle, meaning that solid security doesn’t require the whole process to come to a halt.

  3. **You’re migrating to the cloud (or considering it).** Moving to the cloud often means bringing on new development processes, tools, and systems. It’s a perfect time to make processes faster and more secure — and DevSecOps could make that a lot easier.


  ## Creating a DevSecOps culture


  Making the shift to a DevSecOps approach helps organizations address security threats in real time, but the change won’t happen overnight. The right mindset is just as important as the right toolset in making the leap. Here are five ways to prepare yourself (and your team) to embrace DevSecOps:


  1. **Remember that security and security professionals are valuable assets, not bottlenecks or barriers.** Don’t lose sight of the wider perspective: a vulnerability that isn’t detected until later in the software development process is going to be much harder and more expensive to fix.

  2. **Work in small iterations.** By delivering code in small chunks, you’ll be able to detect vulnerabilities more quickly.

  3. **Allow everyone to contribute.** Establish a norm where everyone can comment and suggest improvement to code and processes. Encouraging everyone on the team to submit changes jumpstarts collaboration and makes it everyone’s responsibility to improve the process.

  4. **Always be ready for an audit.** Ensure that everyone on the team understands the importance of compliance and establish norms for collecting and updating compliance information.

  5. **Train everyone on security best practices.** Ensure that your development and operations teams are well trained in secure development by providing detailed security guidelines and hands-on training.



  ## Getting started


  Ready to see how GitLab can help you get started with DevSecOps?


  Our [DevSecOps Solution](https://about.gitlab.com/solutions/dev-sec-ops/){:data-ga-name="dev-sec-ops"}{:data-ga-location="body"}. page has all of the details, along with a Free Trial offer for our Ultimate tier of capabilities.


  ## Assess your DevSecOps maturity


  We have developed a simple assessment that uses twenty questions to help you determine where you excel and areas for improvement. A helpful guide suggests steps to get you started.


  [Assess yourself  →](/quiz/devsecops-html/){:data-ga-name="Assess yourself"}{:data-ga-location="body"}.
benefits_title: Benefits of DevSecOps
benefits:
  - image: /images/icons/time-icon2.svg
    title: Speed
    description: Developers can remediate vulnerabilities while they’re coding,
      which teaches secure code writing and reduces back and forth during
      security reviews.
  - title: Collaboration
    description: Encouraging a security mindset across your app dev team aligns
      goals with security and encourages employees to work with others outside
      their functional silo.
    image: /images/icons/first-look-influence.svg
  - title: Efficiency
    description: DevSecOps saves time, money, and employee resources over every
      launch and iteration - all valuable assets to IT and security teams
      suffering from <a
      href="https://go.forrester.com/blogs/security-risk-2019-cybersecuritys-staffing-shortage-is-self-inflicted/">skills</a>
      and budget shortages.
    image: /images/icons/trending-icon2.svg
cta_banner:
  - title: Manage your toolchain before it manages you
    body: Visible, secure, and effective toolchains are difficult to come by due to
      the increasing number of tools teams use, and it’s placing strain on
      everyone involved. This study dives into the challenges, potential
      solutions, and key recommendations to manage this evolving complexity.
    cta:
      - text: Read the study
        url: https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/
resources_title: Resources
resources_intro: >-
  Here is a list of resources on DevSecOps that we find to be particularly
  helpful. We would love to get your recommendations on books, blogs, videos,
  podcasts and other resources that offer valuable insight into best practices.


  Please share your favorites with us by tweeting us [@GitLab!](https://twitter.com/gitlab)
resources:
  - url: https://www.youtube.com/watch?v=4ROYvNfRZVU&feature=emb_logo
    title: Finding Bugs with Coverage Guided Fuzz Testing (DevSecOps)
    type: Video
  - url: https://page.gitlab.com/resources-article-RightPlatformDoD.html
    title: Best Practices for a DoD DevSecOps Culture
    type: Articles
  - title: Citizen-Centric Resiliency In Challenging Times
    url: https://page.gitlab.com/webcast-citizen-centric-resiliency.html
    type: Webcast
suggested_content:
  - url: /blog/2019/08/12/developer-intro-sast-dast/
  - url: /blog/2019/09/03/developers-write-secure-code-gitlab/
  - url: /blog/2019/09/16/security-testing-principles-developer/
